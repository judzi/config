# Docker config
alias d="docker"
alias dps="d ps"
alias drmf="d rm -f"
alias dl="d logs"
alias de="d exec -it"

# Default config
alias c="clear"
alias ..="cd .."
alias ll="ls -l"
alias la="ls -la"
alias profile="${EDITOR:-nano} +120 ~/.zshrc && source ~/.zshrc && echo ZSH config edited and reloaded."
alias grep="grep --color"
alias ip="curl ifconfig.co"

# Git control
alias g="git"
alias ga="g add"
alias gaf="ga ."
alias gs="g status"
alias gc="g checkout"
alias gcrp="g cherry-pick"
alias gl="g log"
alias gmr="g merge"
alias gp="g pull"
alias gps="g push"
alias gpsf="gps --force"
alias gr="g reset"
alias grh="gr --hard"
alias grhm="grh origin/master"
alias grb="g rebase"
alias grbc="grb --continue"
alias grba="grb --abort"
alias gst="g stash"
alias gstp="gst pop"
alias gsta="gst apply"
alias gf="g fetch"
alias gcm="g commit"
alias gcma="gcm --amend"
alias gb="g branch"
alias gcl="g clone"
alias gcp="g cherry-pick"
alias tg='$(create_release_tag)'
alias gconfig='$(git_config)'

# Git branch in prompt.
parse_git_branch() {
  git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'
}

# Git create release tag
create_release_tag() {
  git fetch --tags origin &> /dev/null
  DATE=$(date +"%Y-%m-%d")
  TAG_PREFIX="${DATE}."
  NEXT_TAG_ID=$(($(git tag -l "${TAG_PREFIX}*" | wc -l)+1))
  NEXT_TAG="${TAG_PREFIX}${NEXT_TAG_ID}"

  git tag "${NEXT_TAG}" && git push origin "${NEXT_TAG}"
}

git_config() {
  git config user.name "Juraj Hrenák"
  git config user.email "judzinator@gmail.com"
}

#uczexpo() {
 # CONTAINER_ID=$(ssh expo@expo.xp "docker ps -aqf name=${1}")
  #ssh expo@expo.xp docker exec -t ${CONTAINER_ID} bash
#}

#qpdf3 () {qpdf --linearize --force-version=1.3 ${1} _${1};  mv _${1} ${1};}

# Usetreno.cz Development
alias ucz11="ssh usetreno@77.93.209.11"
alias ucz12="ssh usetreno@77.93.209.12"
alias ucz13="ssh usetreno@77.93.209.13"
alias ucz14="ssh usetreno@77.93.209.14"
alias uczcert="ssh root@77.93.209.10"
alias uczexpo="ssh expo@expo.xp"
alias uczcron="ssh cron@cron.adfinance.cz"
alias uczw1="ssh root@whale-1.adfinance.cz"
alias uczw2="ssh root@whale-2.adfinance.cz"
alias uczw3="ssh root@whale-3.adfinance.cz"
alias uczw4="ssh expo@whale-4.adfinance.cz"
alias uczd="ssh usetreno@172.16.209.27"
alias uczacc="ssh usetreno@172.16.209.28"
alias uczmail="ssh usetreno@77.93.209.4"
alias uczstrapi="ssh usetreno@172.16.209.40"
alias uczstrapiacc="ssh usetreno@172.16.209.42"
alias uczstrapidev="ssh usetreno@172.16.209.41"

alias uczdev="cd ~/Development/usetreno.cz/"
alias uczstart="uczdev && cd docker-dev/ && ./adf_start.sh"
alias uczstop="uczdev && cd docker-dev/ && ./adf_stop.sh"
alias uczrestart="uczstop && uczstart"
alias sov="uczdev && cd sovicka3"
alias sovstart="sov && uczsovsyncstart && uczsovstart && uczsovsyncfix"
alias sovstop="sov && uczsovstop && uczsovsyncstop"
alias sovphp="docker exec -it sovicka3_php bash"
alias uczsovsyncfix="docker exec -it sovicka-sync chmod 777 -R app_sync/app/cache"
alias uczsovsyncstart="docker-sync start"
alias uczsovsyncstop="docker-sync stop"
alias uczsovstart="docker compose -f docker-compose-mac.yml up -d"
alias uczsovstop="docker compose -f docker-compose-mac.yml down"
alias uczcms="uczdev && cd cms"
alias cmsstart="uczcms && uczcmssyncstart && uczcmsstart && uczcmssyncfix"
alias cmsstop="uczcms && uczcmsstop && uczcmssyncstop"
alias cmsphp="docker exec -it cms_php bash"
alias uczcmssyncfix="docker exec -it cms-sync chmod 777 -R app_sync/app/cache"
alias uczcmssyncstart="docker-sync start"
alias uczcmssyncstop="docker-sync stop"
alias uczcmsstart="docker compose -f docker-compose-mac.yml up -d"
alias uczcmsstop="docker compose -f docker-compose-mac.yml down"
#alias uczphp="docker exec -it adf_php7 bash"
#alias uczphp5="uczdev && cd docker-dev/ && ./adf_enter.sh"

uczskeletondown () {
  scp -r "expo@expo.xp:/media/adfdata/database_skeleton/$(printf %q "$(ssh expo@expo.xp 'ls -t /media/adfdata/database_skeleton | head -1')")" /Users/judzi/Development/usetreno.cz/adfdata/database_skeleton/
}
alias uczskeleton="uczskeletondown"

# UCZ PHP74 debug
debug74 () {
  DEFAULT_PATH="/Users/judzi/Development/usetreno.cz/hello"
  docker run -p 8088:8080 --rm --name hello_world -v $INPUT_PATH:/var/www gitlab.adfinance.cz:4567/docker/base-php7:php74 php -S 0.0.0.0:8080 -t /var/www
}

alias debug="debug74"

# The Fuck config
#eval "$(thefuck --alias)"

# Judzi Development
alias vision="ssh root@80.211.215.27"
alias pi="ssh pi@raspberry-hb.local"
alias pi2="ssh pi@pi2.local"
alias pi3d="ssh pi@octopi.local"

alias dev="cd ~/Development/judzi/"
alias devbase="dev && cd dev-base/"
alias devstart="devbase && docker compose up -d"
alias devstop="devbase && docker compose stop"

alias tutorstart="dev && cd tutorka && docker-sync-stack start"
alias tutorphp="docker exec -it tutorka_php sh"
alias tutorapistart="dev && cd tutorka-api && docker-sync-stack start"

#function netfuck --description 'When network is fuckedup'
#    nmcli networking off
#    sleep 10
#    nmcli networking on
#end
