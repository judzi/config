#!/usr/bin/env bash

BRANCHES_TO_SKIP=(acc dev master)
BRANCH_NAME=$(git symbolic-ref --short HEAD)
BRANCH_EXCLUDED=$(printf "%s\n" "${BRANCHES_TO_SKIP[@]}" | grep -c "^$BRANCH_NAME$")

read -p "Commit to ticket/fix/hotfix [$BRANCH_NAME]: " branch
PREFIX=${branch:-$BRANCH_NAME}

MESSAGE=""
while [[ $MESSAGE = "" ]]; do
  read -p "Enter commit message: " MESSAGE
done

if [[ $PREFIX == "HOTFIX" ]] || [[ $PREFIX == "FIX" ]]; then
  $PREFIX="${PREFIX}:"
fi
git commit -m"$PREFIX $MESSAGE"
