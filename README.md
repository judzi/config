# Setup config

## Bash
Add config lines to `~/.bashrc`

## ZSH
Add config lines to `~/.zshrc`

## Bash on MacOS
Add config lines to `~/.bash_profile`
